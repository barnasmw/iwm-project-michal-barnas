from django.db import models
from django.contrib.auth.models import User


class Temperature(models.Model):
    temp = models.DecimalField(max_digits=3, decimal_places=1, default=10.0)

    def __str__(self):
        return str(self.temp)


class Light(models.Model):
    light_state = models.BooleanField(default=False)

    def __str__(self):
        return str(self.light_state)


class Bulbulator(models.Model):
    bulbulator_state = models.BooleanField(default=False)

    def __str__(self):
        return str(self.bulbulator_state)


class Feeding(models.Model):
    notif_title = 'Last fish feeding'
    date_fed = models.DateTimeField(auto_now=True)
    by_whom = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.notif_title


class Waterchange(models.Model):
    notif_title = 'Last time water was changed'
    date_waterchanged = models.DateTimeField(auto_now=True)
    by_whom = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.notif_title
