from django.contrib import admin
from .models import Temperature
from .models import Light
from .models import Bulbulator
from .models import Feeding
from .models import Waterchange

admin.site.register(Temperature)

admin.site.register(Light)

admin.site.register(Bulbulator)

admin.site.register(Feeding)

admin.site.register(Waterchange)
