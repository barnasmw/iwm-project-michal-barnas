from django.urls import path
from .views import (
    FeedingListView,
    FeedingDetailView,
    FeedingCreateView,
    WaterchangeListView,
    WaterchangeDetailView,
    WaterchangeCreateView,
)
from . import views as aq_views

app_name = 'aquarium'
urlpatterns = [
    path('', aq_views.home, name='home'),

    path('feeding-log/', FeedingListView.as_view(), name='feeding-log'),
    path('feeding/<int:pk>/', FeedingDetailView.as_view(), name='feeding-detail'),
    path('feeding-log/new/', FeedingCreateView.as_view(), name='feeding-create'),

    path('waterchange-log/', WaterchangeListView.as_view(), name='waterchange-log'),
    path('waterchange/<int:pk>/', WaterchangeDetailView.as_view(), name='waterchange-detail'),
    path('waterchange-log/new/', WaterchangeCreateView.as_view(), name='waterchange-create'),

    path('increase/', aq_views.increase, name='increase'),
    path('decrease/', aq_views.decrease, name='increase'),

    path('lighton/', aq_views.lighton, name='lighton'),
    path('lightoff/', aq_views.lightoff, name='lightoff'),

    path('bulbulatoron/', aq_views.bulbulatoron, name='bulbulatoron'),
    path('bulbulatoroff/', aq_views.bulbulatoroff, name='bulbulatoroff'),


]
