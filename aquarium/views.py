from django.shortcuts import render, redirect
from datetime import datetime
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import F
from .models import Temperature, Light, Bulbulator, Feeding, Waterchange



def home(request):
    context = {
        'title': 'Your Aquarium\'s Control Panel',
        'feedings': Feeding.objects.order_by('-date_fed')[:1],
        'water_changes': Waterchange.objects.order_by('-date_waterchanged')[:1],
        'date': datetime.now(),
        'temperature': Temperature.objects.get(pk=1),
        'light': Light.objects.get(pk=1),
        'bulbulator': Bulbulator.objects.get(pk=1),
    }

    return render(request, 'aquarium/home.html', context=context)


class FeedingListView(ListView):
    model = Feeding
    template_name = 'aquarium/feeding-log.html'
    context_object_name = 'feedings'
    ordering = ['-date_fed']
    paginate_by = 7


class FeedingDetailView(DetailView):
    model = Feeding


class FeedingCreateView(LoginRequiredMixin, CreateView):
    model = Feeding
    fields = '__all__'
    success_url = '/feeding-log/'

    def form_valid(self, form):
        form.instance.by_whom = self.request.user
        return super().form_valid(form)


class WaterchangeListView(ListView):
    model = Waterchange
    template_name = 'aquarium/waterchange-log.html'
    context_object_name = 'waterchanges'
    ordering = ['-date_waterchanged']
    paginate_by = 7


class WaterchangeDetailView(DetailView):
    model = Waterchange


class WaterchangeCreateView(LoginRequiredMixin, CreateView):
    model = Waterchange
    fields = '__all__'
    success_url = '/waterchange-log/'

    def form_valid(self, form):
        form.instance.by_whom = self.request.user
        return super().form_valid(form)


def feeding_log(request):
    context = {
        'title': 'Feeding Log',
    }
    return render(request, 'aquarium/feeding-log.html', context=context)


def increase(request):
    temp = Temperature.objects.update(temp=F('temp') + 1)
    return redirect('/')


def decrease(request):
    temp = Temperature.objects.update(temp=F('temp') - 1)
    return redirect('/')


def lighton(request):
    light_state = Light.objects.update(light_state=True)
    return redirect('/')


def lightoff(request):
    light_state = Light.objects.update(light_state=False)
    return redirect('/')


def bulbulatoron(request):
    bulbulator_state = Bulbulator.objects.update(bulbulator_state=True)
    return redirect('/')


def bulbulatoroff(request):
    bulbulator_state = Bulbulator.objects.update(bulbulator_state=False)
    return redirect('/')
